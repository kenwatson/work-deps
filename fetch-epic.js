const https = require('https');

let epic = {}
const epicPromise = new Promise((resolve, reject) => {
	https.get({
		hostname: 'api.app.shortcut.com',
		port: 443,
		path: `/api/v3/epics/${process.argv[2]}`,
		method: 'GET',
		headers: {
			'Shortcut-Token': process.env.SHORTCUT_API_TOKEN
		}
	}, (res) => {
		res.setEncoding("utf8");
		let body = "";
		res.on("data", data => body += data);
		res.on("end", () => {
			epic = JSON.parse(body);
			resolve();
		});
	})
})

let members = {}
const memberPromise = new Promise((resolve, reject) => {
	https.get({
		hostname: 'api.app.shortcut.com',
		port: 443,
		path: '/api/v3/members',
		method: 'GET',
		headers: {
			'Shortcut-Token': process.env.SHORTCUT_API_TOKEN
		}
	}, (res) => {
		res.setEncoding("utf8");
		let body = "";
		res.on("data", data => body += data);
		res.on("end", () => {
			body = JSON.parse(body);
			body.forEach(m => {
				members[m.id] = {
					"id": m.id,
					"name": m.profile.name,
					"icon": m.profile.display_icon ? m.profile.display_icon.url : null
				}
			})

			resolve();
		});
	})
})

Promise.all([epicPromise, memberPromise]).then(() =>
	https.get({
		hostname: 'api.app.shortcut.com',
		port: 443,
		path: `/api/v3/epics/${process.argv[2]}/stories`,
		method: 'GET',
		headers: {
			'Shortcut-Token': process.env.SHORTCUT_API_TOKEN
		}
	}, (res) => {
		res.setEncoding("utf8");
		let body = "";
		res.on("data", data => body += data);
		res.on("end", () => {
			let nodes = []
			let edges = []

			body = JSON.parse(body);
			body.forEach(s => {
				if (s.archived) return;

				let node = {
					"id": s.id.toString(),
					"url": s.app_url,
					"description": s.name,
					"last_modified": s.updated_at,
					"type": s.story_type,
                    "state": s.iteration_id ? 'scheduled' : 'unscheduled',
                    "started": s.started.toString(),
					"status": s.completed ? "completed" : s.blocked ? "blocked" : s.started ? "active" : "pending",
					"owner": s.owner_ids.length === 0 ? null : members[s.owner_ids[0]],
					"size": s.estimate ? s.estimate : 0,
					"labels": s.labels ? s.labels.map(l => l.name) : [],
					"requester": members[s.requested_by_id]
				}
				nodes.push({"data": node});

				s.story_links.forEach(l => {
					// console.log(s.id, l.entity_type, l.type, l.verb, l.subject_id, l.object_id)
					if (l.entity_type === 'story-link' && l.type === 'subject' && l.verb === 'blocks') {
						let edge = {
							"source": l.object_id.toString(),
							"target": l.subject_id.toString(),
						}
						edges.push({"data": edge});
					}
				});
			})

			const result = {
				"title": epic.name,
				"elements": {
					"nodes": nodes,
					"edges": edges
				}
			}

			console.log(JSON.stringify(result, null, 2))
		});
	})
)
