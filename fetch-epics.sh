#!/bin/bash

mkdir -p public

echo '<ul>' > public/index.html

epics=$1
for epic in ${epics//,/ }; do
  echo "Fetching epic ${epic}"
  echo "const workData = " > ./work-data.tmp.js && node fetch-epic.js ${epic} >> ./work-data.tmp.js && mv ./work-data.tmp.js ./src/work-data.js
  cp -r ./src ./public/${epic}

  echo "<li><a href='./${epic}/'>${epic}</a></li>" >> public/index.html
done

echo '</ul>' >> public/index.html
