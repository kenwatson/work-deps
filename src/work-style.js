const STATUS = {
	'unknown': 'fuchsia', // not a real status
	'active': 'yellow',
	'blocked': 'red',
	'conflict': 'orange',
	'parked': 'blue',
	'completed': 'limegreen',
	'pending': 'cyan',
	'ready': 'cyan' // pseudo-status for a pending task with only completed dependencies
};

const WORK_STYLE = cytoscape.stylesheet()
	.selector('node')
	.css({
		'transition-property': 'height, width, opacity',
		'transition-duration': '0.25s',
		'height': 100,
		'width': 100,
		'border-color': '#000',
		'border-width': 8,
		'background-color': STATUS.unknown,
		// slightly offset the avatar, so we can still see the background colour
		'background-width': '100%',
		'background-height': '100%',
		'shape': 'barrel'
	})
	.selector('edge')
	.css({
		'curve-style': 'bezier',
		'width': 2.5,
		'line-color': 'grey',
		'arrow-scale': 1.5,
		'source-arrow-shape': 'triangle',
		'target-arrow-shape': 'tee',
		'target-arrow-color': 'grey'
	})
	.selector('node[state="unscheduled"]')
	.css({
		'opacity': '0.5',
	})
	.selector('node.stateOverride')
	.css({
		'opacity': '1',
	})
	.selector('node[status="active"]')
	.css({
		'background-color': STATUS.active,
		'border-color': STATUS.active,
	})
	.selector('node[status="blocked"]')
	.css({
		'background-color': STATUS.blocked
	})
    .selector('node[status="blocked"][started="true"]')
    .css({
        'shape': "round-octagon",
		'background-color': STATUS.conflict,
		'border-color': STATUS.conflict
    })
	.selector('node[status="parked"],node.parked')
	.css({
		'background-color': STATUS.parked,
		'border-style': 'dashed'
	})
	.selector('node[status="completed"]')
	.css({
		'background-color': STATUS.completed
	})
	.selector('node[status="pending"]')
	.css({
		'background-color': STATUS.pending
	})
	.selector('node[status="ready"]')
	.css({
		'background-color': STATUS.ready
	})
	.selector('node.hilite')
	.css({
		// 'border-color': 'red',
		// 'border-width': 5
	})
	.selector('edge.hilite')
	.css({
		'line-color': 'black',
		'width': 6,
		'source-arrow-color': 'black',
		'target-arrow-color': 'black'
	})
	.selector('node[size=1]')
	.css({
		'height': 60,
		'width': 60,
	})
	.selector('node[size=2]')
	.css({
		'height': 80,
		'width': 80,
	})
	.selector('node[size=5]')
	.css({
		'height': 120,
		'width': 120,
	})
	.selector('node[size=8]')
	.css({
		'height': 140,
		'width': 140,
	})
	.selector('node[size=13]')
	.css({
		'height': 160,
		'width': 160,
	})
	.selector('node[size=20]')
	.css({
		'height': 180,
		'width': 180,
	})
;
