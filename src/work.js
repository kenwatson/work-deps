const defaultLayout = {
	name: 'dagre',
	rankDir: "RL",
	// for options see: https://github.com/cytoscape/cytoscape.js-dagre#api
	padding: 15,
	nodeSep: 40,
	rankSep: 220,
	edgeSep: undefined,
	ranker:  undefined, // ranking algorithm: 'network-simplex', 'tight-tree' or 'longest-path'
	animate: false,
	animationDuration: 1500
};

const cy = cytoscape({
	container: document.getElementById('cy'),

	boxSelectionEnabled: false,
	autounselectify: true,

	style: WORK_STYLE,

	elements: workData.elements,

	layout: defaultLayout
});

const tip = document.querySelector("#tip");

const popupItem = function(node) {
	const data = node.data()
	tip.style = 'display: none';

	const type = data.type
		? `<span class="type"><img src="types/${data.type}.png" alt="${data.type}" title="${data.type}"/></span>`
		: '';

	const header = `<span class="workitem">${type} [${data.id}]</span> <span class="title">${data.description}</span><hr/>`;
	const status = `<section class="status ${data.status}">${data.status}</section>`;
	let size = '';
	if (data.size) size = `<section class="size size_${data.size}">Size: ${data.size}</section>`;
	const requester = person(data.requester, 'requester');
	const owner = person(data.owner, 'owner');
	const modified = `<section class="updated">Updated: ${data.last_modified}</section>`;
	const link = `<a href="${data.url}">Open in Shortcut</a>`;
	const labels = data.labels.length > 0
		? `Labels: ${data.labels.join()}`
		: '';

	const meta = `<div class="meta">${status}${link}${size}${requester}${owner}${labels}${modified}</div>`;

	const blockingItems = []
	const blocksItems = []
	node.connectedEdges(el => {
		if (el.target() === node) blockingItems.push(el.source().data())
		if (el.source() === node) blocksItems.push(el.target().data())
	});

	let blocking = ""
	if (blockingItems.length > 0) {
		blocking = "<div>Blocking:"
		blocking += "<ul>"
		blocking += blockingItems.map(d => `<li>[${d.id}] ${d.description}`).join('')
		blocking += "</ul>"
	}

	let blocks = ""
	if (blocksItems.length > 0) {
		blocks = "<div>Depends on:"
		blocks += "<ul>"
		blocks += blocksItems.map(d => `<li>[${d.id}] ${d.description}`).join('')
		blocks += "</ul>"
	}

	popup(header + meta + blocking + blocks);
}

const popup = function(content) {
	uglipop({
		class: 'popup',
		source: 'html',
		content: content
	});
}

// render an avatar for the responsible person
const person = function(user, title) {
	const personTitle = title.charAt(0).toUpperCase() + title.substring(1).toLowerCase();
	let avatar = `<section class='owner'>${personTitle}: `;
	if (user) {
		avatar += `<img src="people/${user.id}.png" alt="${user.name}" title="${user.name}"/> `;
		avatar += user.name;
	} else {
		avatar += "(nobody)";
	}
	avatar += "</section>";

	return avatar;
}

// --- utility functions

// recursively adds or removes styles to a tree of nodes linked by their parents
const parentEdges = function(node, plusStyle, minusStyle, visited = new Set()) {
	// check for cycles
	if (visited.has(node)) return;
	else visited.add(node);
	// update styles
	node.connectedEdges(el => {
		if (el.target() === node) {
			if (plusStyle) {
				el.addClass(plusStyle);
				el.connectedNodes(n => n.addClass(plusStyle));
			}
			if (minusStyle) {
				el.removeClass(minusStyle);
				el.connectedNodes(n => n.removeClass(minusStyle));
			}

			parentEdges(el.source(), plusStyle, minusStyle, visited);
		}
	});
}

// recursively adds or removes styles to a tree of nodes linked by their children
const childEdges = function(node, plusStyle, minusStyle, visited = new Set()) {
	// check for cycles
	if (visited.has(node)) return;
	else visited.add(node);
	// update styles
	node.connectedEdges(el => {
		if (el.source() === node) {
			if (plusStyle) {
				el.addClass(plusStyle);
				el.connectedNodes(n => n.addClass(plusStyle));
			}
			if (minusStyle) {
				el.removeClass(minusStyle);
				el.connectedNodes(n => n.removeClass(minusStyle));
			}

			childEdges(el.target(), plusStyle, minusStyle, visited);
		}
	});
}

const progressBar = function(parent, progress) {
	while (parent.hasChildNodes()) {
		parent.removeChild(parent.lastChild());
	}

	// create holders for count based progress vs time base progress
	const countPart = document.createElement('div');
	parent.appendChild(countPart);

	// create a defined order to statues and then fill with the remaining
	const ordered = { };
	const preferential = [ "completed", "active", "pending", "blocked" ];
	for (const element of preferential) {
		let k = element;
		if (progress.status[k]) ordered[k] = 0;
	}
	for (const k in progress.status) {
		if (!ordered.hasOwnProperty(k)) ordered[k] = 0;
	}

	let title = "";

	// right, now build a progress bar in line with the above status order
	for (const k in ordered) {
		// count
		const part = document.createElement('div');
		const perc = (progress.status[k].count / progress.total.count * 100);
		part.style.display = "inline-block";
		part.style.backgroundColor = STATUS[k];
		part.style.width = `${perc}%`;
		part.style.height = "100%";
		ordered[k] = Math.round(perc);
		countPart.appendChild(part);
		title += `${k}: ${ordered[k]}%\n`;
	}
	parent.title = title.trim();
}


// -- document query functions

document.getElementById("controls-toggle").addEventListener('click', () => {
	document.getElementById("controls").classList.toggle("hide");
});

document.getElementById("export").addEventListener('click', () => {
	const link = document.createElement("a");
	link.download = 'work.png';
	link.href = cy.png({
		scale: 2,
		bg: 'white'
	});
	document.body.appendChild(link);
	link.click();
	document.body.removeChild(link);
});

document.getElementById("layout").addEventListener('click', () => {
	defaultLayout.animate = true;
	if (defaultLayout.rankDir === 'RL') defaultLayout.rankDir = 'TB';
	else defaultLayout.rankDir = 'RL';
	cy.layout(defaultLayout).run();
});

document.getElementById("state").addEventListener('click', () => {
	cy.collection('node').forEach(n => {
		n.toggleClass("stateOverride");
	});
});

// -- cy event handlers

cy.on('mouseover', 'node', function() {
	this.addClass('hilite');
	parentEdges(this, 'hilite');
	childEdges(this, 'hilite');

	const pos = this.renderedPoint();

	// handle tool-tips when nodes are close to the right hand edge

	// start by setting the tip's content, so we know how big it probably is
	tip.innerHTML = `<span class="status ${this.data().status}">[${this.data().status}]</span> [${this.data().id}] ${this.data().description}`;

	// but to calculate an element's width, it has to be visible and attached to the DOM
	tip.style = 'left: -1000px; top: -1000px';

	// so now that it's "visible", we can use its width to calculate where it should ACTUALLY be
	if (pos.x + this.renderedOuterWidth() + tip.offsetWidth < cy.width()) {
		tip.style = 'left: ' + (pos.x + (this.renderedOuterWidth() / 2)) + 'px;' +
			'top: ' + (pos.y - (this.renderedOuterHeight() / 2)) + 'px';
	} else {
		tip.style = 'right: ' + (cy.width() - (pos.x - (this.renderedOuterWidth() / 2))) + 'px;' +
			'top: ' + (pos.y - (this.renderedOuterHeight() / 2)) + 'px';
	}

	// yay HTML and javascript
});

cy.on('mouseout', 'node', function() {
	this.removeClass('hilite');
	parentEdges(this, false, 'hilite');
	childEdges(this, false, 'hilite');

	tip.style = 'display: none';
});

cy.on('click', 'node', function() {
	popupItem(this);
});

cy.ready(function() {
	// sum up total count and time
	const progress = {
		total: {
			count: 0
		},
		status: {}
	};

	const types = {
		"bug": new Promise((res) => {
			const img = document.createElement("img");
			img.src = "types/bug.png";
			img.addEventListener("load", () => res(img));
		}),
		"chore": new Promise((res) => {
			const img = document.createElement("img");
			img.src = "types/chore.png";
			img.addEventListener("load", () => res(img));
		}),
		"feature": new Promise((res) => {
			const img = document.createElement("img");
			img.src = "types/feature.png";
			img.addEventListener("load", () => res(img));
		})
	};

	// find nodes which may be worked on
	cy.collection('node').forEach(n => {
		const c = document.createElement("canvas");
		c.width = 128;
		c.height = 128;
		const ctx = c.getContext("2d");
		ctx.font = 'bold 56px sans-serif';
		ctx.fillStyle = 'white';
		ctx.strokeStyle = 'black';
		ctx.lineWidth = 3;
		ctx.textBaseline = 'bottom';
		ctx.textAlign = 'right';
		if (n.data().owner && n.data().owner.id) {
			if (n.data().type) {
				const img = document.createElement("img");
				img.src = `people/${n.data().owner.id}.png`;
				img.addEventListener("load", () => {
					ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, c.width, c.height);
					types[n.data().type].then(typ => {
						if (n.data().size) {
							ctx.fillText(n.data().size.toString(), c.width - 10, c.height / 2);
							ctx.strokeText(n.data().size.toString(), c.width - 10, c.height / 2);
						}
						ctx.drawImage(typ, 0, 0, typ.width, typ.height, c.width / 2, c.height / 2, c.width / 2, c.height / 2);
						n.style("background-image", "url(" + c.toDataURL() + ")");
					});
				});
			} else {
				n.style("background-image", "url(people/" + n.data().owner.id + ".png)");
			}

			n.style("border-color", STATUS[n.data().status]);
			n.style("border-width", 8);
		} else if (n.data().type) {
			types[n.data().type].then(typ => {
				if (n.data().size) {
					ctx.fillText(n.data().size.toString(), c.width - 10, c.height / 2);
					ctx.strokeText(n.data().size.toString(), c.width - 10, c.height / 2);
				}
				ctx.drawImage(typ, 0, 0, typ.width, typ.height, c.width / 2, c.height / 2, c.width / 2, c.height / 2);
				n.style("background-image", "url(" + c.toDataURL() + ")");
			});
		}

		if (n.data().labels.indexOf("on-hold") > -1) n.addClass("parked");

		// count and time summary for this status, and totals
		if (!progress.status[n.data().status]) {
			progress.status[n.data().status] = {
				count: 0
			};
		}

		progress.status[n.data().status].count++;

		progress.total.count++;

		const canWork = n.data().status === 'pending' && n.connectedEdges(el => {
			return !el.target().same(n) && (!el.target().same(n) && el.target().data().status !== "completed");
		}).length === 0;

		if (canWork) n.data().status = 'ready';
	});

	setTimeout(() => cy.forceRender(), 100); // we updated the style, but it doesn't re-render automatically

	// render the progress bar thing
	progressBar(document.getElementById("progress"), progress);

	// set the document title
	document.getElementsByTagName("title").item(0).innerText = workData.title;
});
